package stringcalculatorkata;

public class NegativesValuesNotAllowed extends Exception {
    public NegativesValuesNotAllowed() {
    }

    public NegativesValuesNotAllowed(String message) {
        super(message);
    }
}
