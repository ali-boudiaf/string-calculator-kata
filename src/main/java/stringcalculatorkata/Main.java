package stringcalculatorkata;

import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            int result = SumOfValuesInString.sumOfValuesInString(scanner.nextLine());
            System.out.println("Sum of values in string : " + result);
        } catch (NegativesValuesNotAllowed e) {
            e.printStackTrace();
        }
    }
}
