package stringcalculatorkata;


import java.util.Arrays;

public class SumOfValuesInString {

    public static int sumOfValuesInString(String stringContainValues) throws NegativesValuesNotAllowed {

        if (stringContainValues.isEmpty()) {
            return 0;
        }

        String[] splitString = new ConvertStringToTabOfValues().convertStringToTabOfValues(stringContainValues);

        new HasNegativeValues().hasNegativeValues(splitString);

        return getSum(splitString);
    }

    static private int getSum(String[] splitString) {
        return Arrays.stream(splitString)
                .mapToInt(Integer::parseInt)
                .sum();
    }
}
