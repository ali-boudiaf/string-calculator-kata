package stringcalculatorkata;

import java.util.Arrays;


/**
 * Vérifie si la chaine contient des valeurs négatives
 * Une exception est levé si la chaine contient une valeur négative
 */
public class HasNegativeValues {
    public void hasNegativeValues(String[] splitString) throws NegativesValuesNotAllowed {
        var reduceWithNegativesValues = Arrays.stream(splitString)
                .mapToInt(Integer::parseInt)
                .filter(integer -> integer < 0)
                .mapToObj(Integer::toString)
                .reduce((a, s) -> a + ' ' + s);
        //System.out.println(reduceWithNegativesValues);

        if (reduceWithNegativesValues.isPresent()) {
            throw new NegativesValuesNotAllowed("negatives not allowed " + reduceWithNegativesValues.get());

        }
    }
}
