package stringcalculatorkata;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Transforme une chaine de caractères en tableau.
 *
 * Un délimiteur par défaut est défini.
 * La librairie regex est utilisé pour trouver un potentiel nouveau délimiteur
 */
public class ConvertStringToTabOfValues {

    public String[] convertStringToTabOfValues(String stringContainValues) {
        String delimiter = "\n|,";
        Pattern pattern = Pattern.compile("^//([^\n])\n(.*)$", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(stringContainValues);
        if (matcher.find()) {
            delimiter = "\n|" + matcher.group(1);
            stringContainValues = matcher.group(2);
        }
        return stringContainValues.split(delimiter);
    }
}
