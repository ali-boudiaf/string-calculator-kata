package stringcalculatorkata;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SumOfValuesInStringTest {
    @Test
    public void shouldReturnCorrectAnswerWhen2Values() throws Exception {
        Assertions.assertEquals(3, new SumOfValuesInString().sumOfValuesInString("1,2"));
    }

    @Test
    public void shouldReturn0whenStringIsEmpty() throws Exception {
        Assertions.assertEquals(0, new SumOfValuesInString().sumOfValuesInString(""));
    }


    @Test
    public void shouldeReturnCorrectAnswerWithOneValue() throws Exception {
        Assertions.assertEquals(1, new SumOfValuesInString().sumOfValuesInString("1"));
    }

    @Test
    public void shouldReturnCorrectAnswerWithMultipleValues() throws Exception {
        Assertions.assertEquals(10, new SumOfValuesInString().sumOfValuesInString("1,2,3,4"));
    }

    @Test
    public void shouldReturnCorrectAnswerWhenNewLineDelimiterAndDefaultDelimiter() throws Exception {
        Assertions.assertEquals(6, new SumOfValuesInString().sumOfValuesInString("1\n2,3"));
    }

    @Test
    public void shouldReturnCorrectAnswerWithNewDelimiter() throws Exception {
        Assertions.assertEquals(3, new SumOfValuesInString().sumOfValuesInString("//;\n1;2"));
    }

    @Test
    public void shouldReturnCorrectAnswerWhenUsingNewLineDelimiterAndNewDelimiter() throws Exception {
        Assertions.assertEquals(6, new SumOfValuesInString().sumOfValuesInString("//;\n1\n2;3"));
    }

    @Test
    public void shouldThrowIllegalArgumentAndReturnNegativesValues() {
        Exception exception = Assertions.assertThrows(Exception.class, () -> {
            new SumOfValuesInString().sumOfValuesInString("//;\n-1;-6;2");
        });
        Assertions.assertEquals("negatives not allowed -1 -6", exception.getMessage());
    }

}
